// var z = 10
// ;(function(funArg){
//   // var z=20
//   funArg(z)
// })(foo)
// function foo(m=z){
//   console.log(m)
// }
// z=11
// foo()
// console.log('z: ',z)

// var data=[]
// for(let k=0;k<3;k++){
//   data[k]=function(){
//     console.log(k)
//   }
// }
// data[0]()
// data[1]()
// data[2]()


// function fun(n,o){
//   console.log(o)
//   return {
//     fun: function(m){
//       return fun(m, n)
//     }
//   }
// }
// var a=fun(0); //
// a.fun(1);  //
// a.fun(2);
// a.fun(3);
// var b=fun(0).fun(1).fun(2).fun(3)
// undefined   fun(0) = {fun:(m)=>fun(m, 0)}
// 0   fun(1) = {fun: (m)=>fun(m, 1)}
// 1   fun(2) = {fun: (m)=>fun(m, 2)}
// 2

// var c=fun(0).fun(1);c.fun(2);c.fun(3)

// var a = 100
// function test(){
//   var b = 2*a
//   var a = 200
//   var c = a/2
//   console.log(b)
//   console.log(c)
// }
// test()

// var res = (function(a){
//   this.a = a
//   return function(b){
//     return this.a+b
//   }
//  }(function(a,b){
//   return a
// }(1,2)))
// console.log(res(1))

// function sleep(time){
//   return new Promise((resolve)=>setTimeout(()=>resolve(2), time))
// }

// sleep(2000).then(function(value){
//   console.log(value)
// })

// [1,1,2,3,5...]

//斐波那契数列
// function Fibonacci(index) {
//   var result
//   var num1 = 1
//   var num2 = 1
//   if(index === 0 || index === 1){
//     return 1
//   }
//   for(var i=2;i<=index;i++){
//     result = num1 + num2
//     num1 = num2
//     num2 = result
//   }
//   return result
// }
// for(var i=0;i<6;i++){
//   console.log(Fibonacci(i))

// }

//~~~~~~~~不会
// var length = 10;
// function fn() {
//   console.log(this.length);
// }

// var obj = {
//   length: 5,
//   method: function(fn) {
//     // fn();
//     arguments[0]();
//   }
// };
// obj.method(fn, 1);

// function fn(a) {
//   console.log(a);
//   var a = 2;
//   function a() {}
//   console.log(a);
// }

// fn(1);

// var f = true;
// if (f === true) {
//   var a = 10;
// }

// function fn() {
//   var b = 20;
//   c = 30;
// }

// fn();
// console.log(a);
// // console.log(b);
// console.log(c);

// var a = 10;
// a.pro = 10;
// console.log(a.pro + a);

// var s = 'hello';
// s.pro = 'world';
// console.log(s.pro + s);

// function plus(a, b, c) {
//   var result
//   if (arguments.length === 1) {
//     return plus.bind(this, a)
//   }
//   else if (arguments.length === 2) {
//     return plus.bind(this, a, b)
//   }
//   return a + b + c
// }
function plus2() {
  if (arguments.length < 3) return plus2.bind(this, ...arguments)
  return Array.prototype.reduce.call(arguments, (prev, current) => {
    return prev + current
  })
}
console.log(plus2(-1)(1)(3))  //知道参数以及上一个参数  返回函数自己本身

// function f(a) {
//   return (b) => {
//     return (c) => {
//       return a + b + c
//     }
//   }
// }
// console.log(f(-1)(1)(2))

// function fn(x, y) {
//   console.log(arguments)
//   return fn.bind(null, x)
// }

// fn(1)(2)(3)

// function list(){
//   console.log(arguments)
//   return Array.prototype.slice.call(arguments)
// }
// var list1 = list(1,2,3)
// var leadingThirtysevenList = list.bind(this, 37);

// var list2 = leadingThirtysevenList();
// var list3 = leadingThirtysevenList(1, 2, 3);

// console.log(list3)


