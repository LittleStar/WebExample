class Father {
  constructor(money, house) {
    this.money = money
    this.house = house
  }
  study() {
    console.log('prototype study')
  }
}

class Son extends Father {
  constructor(money, house, age) {
    super(money, house)
    this.age = age
  }
}

var s1 = new Son(2000, 100, 25)
console.log(s1)
console.log(s1.study())
console.log(s1.money)


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function Father(money, house) {
//   this.money = money
//   this.house = house
// }
// Father.prototype.study = () => { console.log('prototype study') }
// var f1 = new Father()

// function Son(money, house, age) {
//   Father.call(this, money, house)
//   this.age = age
// }

// Son.prototype = f1
// f1.constuctor = Son
// var s1 = new Son(1000, 100) //一定要在重新指定prototype和constructor之后调用
// console.log(s1)
// console.log(s1.money)
// console.log(s1.study())

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function Father(money, house) {
//   this.money = money
//   this.house = house
// }
// Father.prototype.study = () => { console.log('prototype study') }
// var f1 = new Father(100, 20)

// function Son(money, house, age) {
//   Father.call(this, money, house)
//   this.age = age
// }
// Son.prototype = f1
// Son.prototype.eat = function () {
//   console.log('derived eat')
// }

// f1.constructor = Son
// var s1 = Object.create(f1)
// Son.call(s1, 1000, 10, 20)

// console.log(s1, s1.eat)
