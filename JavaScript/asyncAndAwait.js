function replacePromise1() {
  return new Promise((resolve, reject) => {
    console.log('start')
    resolve(1)
    console.log('end')
  })
}
p=replacePromise1()
p.then((result) => {
  console.log(result)
}).catch((e)=>{console.error(e)})


async function replacePromise2() {
  console.log('start')
  return 1
  console.log('end')
}

replacePromise2().then((result) => {
  console.log(result)
}).catch((e)=>{console.error(e)})

async function run(){
  try{
    var result = await replacePromise2()
    console.log('11',result)
  }catch(e){
    // reject()
    console.error(e)
  }
}
run()
// let a= await 20
// let a
// (()=>{return new Promise.resolve(20)})().then(result=>a=result)

// (()=>{return new Promise((resolve,reject)=>{
//   resolve(20)
// })})().then(result=>a=result)
