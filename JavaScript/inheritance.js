class Father {
  constructor(money, house) {
    this.money = money
    this.house = house
  }
  study() {
    console.log('prototype study')
  }
}

class Son extends Father {
  constructor(money, house, age) {
    super(money, house)
    this.age = age
  }
}

var s1 = new Son(2000, 100, 25)
console.log(s1)
console.log(s1.study())
console.log(s1.money)


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function Father(money, house) {
//   this.money = money
//   this.house = house
// }
// Father.prototype.study = () => { console.log('prototype study') }
// var f1 = new Father()

// function Son(money, house, age) {
//   Father.call(this, money, house)
//   this.age = age
// }

// Son.prototype = f1
// f1.constuctor = Son
// var s1 = new Son(1000, 100) //一定要在重新指定prototype和constructor之后调用
// console.log(s1)
// console.log(s1.money)
// console.log(s1.study())

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function Father(money, house) {
//   this.money = money
//   this.house = house
// }
// Father.prototype.study = () => { console.log('prototype study') }
// var f1 = new Father(100, 20)

// function Son(money, house, age) {
//   Father.call(this, money, house)
//   this.age = age
// }
// Son.prototype = f1
// Son.prototype.eat = function () {
//   console.log('derived eat')
// }

// f1.constructor = Son
// var s1 = Object.create(f1)
// Son.call(s1, 1000, 10, 20)

// console.log(s1, s1.eat)


// 继承
class A {
  constructor() {
    this.name = 'A'
  }
  say() {
    console.log(this.name)
  }
}

class B extends A {
  constructor() { // 写不写默认都会生成
    super() //必须先调用父类构造函数
  }
}
var b = new B
b.say()

// 继承的深入讨论
function Ax(name) {
  // this.name = 'Ax'
  this.name = name
}
Ax.prototype.say = function () {
  console.log(this.name)
}

function Bx(name) {
  Ax.call(this, name)
}
Bx.prototype = new Ax('I am Ax')
var bx = new Bx('I am Bx')
bx.say()
console.log(111, bx.name, Object.getPrototypeOf(bx).name)

// 组合
class A2 {
  constructor() {
    this.name = 'A2'
  }
  say() {
    console.log(this.name)
  }
}

class B2 {
  constructor() {
    this.a2 = new A2()
  }
}

var b2 = new B2()
b2.a2.say()

// Mixin
function mixinAssign(target, ...source) {
  for (var i = 0; i < source.length; i++) {
    var properties = Object.getOwnPropertyNames(source[i])
    properties.forEach((prop) => {
      if (Object.prototype.hasOwnProperty.call(target, prop)) return
      var descriptor = Object.getOwnPropertyDescriptor(source[i], prop)
      Object.defineProperty(target, prop, descriptor)
    })
  }
}

function A3() {
  this.name = 'A3'
}

Object.defineProperty(A3.prototype, 'say', {
  value: function () {
    console.log(this.name)
  },
  writable: true,
  enumerable: false,
  configurable: true
})


class B3 {
  constructor() {
    A3.call(this)
  }
}

mixinAssign(B3.prototype, A3.prototype)
var b3 = new B3()
b3.say()
console.log(Object.getOwnPropertyDescriptor(Object.getPrototypeOf(b3), 'say'))
