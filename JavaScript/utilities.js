// Get all enumerable and non-enumerable properties on prototype chain.

function Person(name) {
  this.name = name
}
Person.prototype.eat = function () {
  console.log('eat')
}
var obj = new Person

function getAllProperties(obj) {
  var allProps = []
  var current = obj
  do {
    var props = Object.getOwnPropertyNames(current)
    props.forEach(prop => {
      if (allProps.indexOf(prop) === -1) allProps.push(prop)
    })
  } while (current = Object.getPrototypeOf(current))
  return allProps
}

function getAllPrototypes(obj) {
  var allPrototypes=[]
  var current = obj
  allPrototypes.push(obj)
  do{
    var currentPrototype = Object.getPrototypeOf(current)
    allPrototypes.push(currentPrototype)
  } while(current = Object.getPrototypeOf(current))
  return allPrototypes
}

// export { getAllProperties, getAllPrototypes }

console.log(getAllProperties(obj))
