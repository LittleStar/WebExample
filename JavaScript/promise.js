// function resolve(value) {
//   console.log(`resolved: ${value}`)
//   return value
// }
// function reject(reason) {
//   console.error(`rejected: ${reason}`)
//   throw reason
// }

// var key = 1

// var promise1 = new Promise((resolve, reject) => {
//   let name = 'p1'
//   console.log(name)
//   if (key) {
//     resolve(name)
//   }
//   reject(name)
//   console.log('end')
// })
// var promise2 = new Promise((resolve, reject) => {
//   let name = 'p2'
//   console.log(name)
//   if (key) {
//     resolve(name)
//   }
//   reject(name)
//   console.log('end')
// })

// var p1 = () => {
//   return new Promise((resolve) => {
//     var name = 'p1'
//     console.log(name)
//     resolve()
//   })
// }
// var p2 = () => {
//   return new Promise((resolve) => {
//     var name = 'p2'
//     console.log(name)
//     resolve()
//   })
// }
// var p3 = () => {
//   return new Promise((resolve) => {
//     var name = 'p3'
//     console.log(name)
//     resolve()
//   })
// }
// var p4 = () => {
//   return new Promise((resolve) => {
//     var name = 'p4'
//     console.log(name)
//     resolve()
//   })
// }
// var p5 = () => {
//   return new Promise((resolve) => {
//     var name = 'p5'
//     console.log(name)
//     resolve()
//   })
// }

// console.log(9999)

// var arr = [p1, p2, p3, p4, p5]

// function chainPromise(arr, index) {
//   let result = wrap2Promise(arr[index]())
//   if (arr.length - 1 === index) {
//     return result
//   }
//   return result.finally(() => chainPromise(arr, index + 1))
// }

// function wrap2Promise(obj) {
//   if (obj instanceof Promise) {
//     return obj
//   }
//   return Promise.resolve().then(() => obj)
// }

// chainPromise(arr, 0).finally(() => process.exit())

//~~~~~~~~~~~~~~~~~~~
// function resolveAfter2Seconds() {
//   return new Promise(resolve => {
//     setTimeout(() => {
//       resolve('resolved');
//     }, 2000);
//   });
// }

// async function asyncCall() {
//   console.log('calling');
//   var result = await resolveAfter2Seconds();
//   console.log(result);
//   // expected output: "resolved"
// }

// asyncCall();

