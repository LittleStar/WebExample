function restProps(obj, ...props) {
  var newObj = {}
  for (var key in obj) {
    var flag = false
    for (var i = 0; i < props.length; i++) {
      if (key == props[i]) {
        flag = true
        break
      }
    }
    if (!flag) {
      newObj[key] = obj[key]
      console.log(newObj[key])
    }
  }
  return newObj
}

function getRestProps(obj, ...props) {
  var newObj = {}
  for (var key in obj) {
    if (props.indexOf(key) !== -1) continue
    newObj[key] = obj[key]
    console.log(newObj[key])
  }
  return newObj
}

var obj = {
  name: 'zs',
  age: 18,
  sex: 'male'
}

console.log(getRestProps(obj, 'sex', 'age', 'c'))