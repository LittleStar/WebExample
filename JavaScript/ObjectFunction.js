
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.eat = function () {
  console.log('eat')
}
var obj = new Person('zs')
var obj1 = new Person('ls', 18)
// console.log(Object.getOwnPropertyNames(obj))//自己身上可枚举不可枚举的属性
// console.log(Object.getOwnPropertyNames(Object.getPrototypeOf(obj)))//返回指定对象的原型


//自身及原型上可枚举和不可枚举的属性
function getProperty(obj) {
  var list = []
  var current = obj
  do {
    var props = Object.getOwnPropertyNames(current) //返回一个由指定对象的所有自身属性的属性名（包括不可枚举属性但不包括Symbol值作为名称的属性）组成的数组。
    props.forEach(prop => {
      if (list.indexOf(prop) === -1) {
        list.push(prop)
      }
    })
  } while (current = Object.getPrototypeOf(current))  //返回指定对象的原型
  return list
}
console.log(getProperty(obj))

//自身的可枚举属性和不可枚举属性拷贝到另一个对象上
// f(target, source)
function getPropertyFn(target, source) {
  var props = Object.getOwnPropertyNames(source) //自身的可枚举属性和不可枚举属性
  props.forEach(prop => {
    target[prop] = source[prop]
  })
  return target
}
// console.log(getPropertyFn({},obj))

//包括原型上的可枚举和不可枚举属性拷贝到另一个对象上
function getPropertyFn1(target, source) {
  var current = source
  do {
    var props = Object.getOwnPropertyNames(current)
    props.forEach(prop => {
      if (!target[prop]) target[prop] = current[prop] //target和source属性重名时，不考虑source
    })
  } while (current = Object.getPrototypeOf(current))
  return target
}
// console.log(getPropertyFn1({},obj))

//多个对象上自身的可枚举属性拷贝到另一个对象上，同名属性覆盖  同assign函数
function assign(target, ...source) {
  for (var i = 1; i < arguments.length; i++) {
    for (var prop in arguments[i]) {
      if (arguments[i].hasOwnProperty(prop)) {
        console.log('prop', prop)
        target[prop] = arguments[i][prop]
      }
    }
    // var props = Object.getOwnPropertyNames(arguments[i]) //可枚举和不可枚举
    // props.forEach(prop=>{
    //   target[prop] = arguments[i][prop]
    // })
  }
  return target
}
// console.log(assign({sex: 'male'},obj,obj1))

//~~~~~~~~~~~~~~~~~~~~~~~~~
function f(y) {
  return this.x + y
}
var o = { x: 1 }
// console.log(f.bind(o)(2))

//实现bind函数
function bind(f, o) {
  return function () {
    return f.call(o, ...arguments)
  }
}
console.log(bind(f, o)(2))